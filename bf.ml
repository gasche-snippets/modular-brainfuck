
module type SEMANTICS = sig
  type mem
  val mem       : mem
  val forward   : mem -> mem
  val backward  : mem -> mem
  val increment : mem -> mem
  val decrement : mem -> mem
  val output    : mem -> mem
  val input     : mem -> mem
  val repeat    : (mem -> mem) -> (mem -> mem)
end

module Brainfuck (S : SEMANTICS) = struct
  type instr = Fwd | Bak | Inc | Dec | Out | Inp | Rep of program
  and program = instr list


  let rec eval p t = List.fold_left (fun t -> function
    | Fwd   -> S.forward   t
    | Bak   -> S.backward  t
    | Inc   -> S.increment t
    | Dec   -> S.decrement t
    | Out   -> S.output    t
    | Inp   -> S.input     t
    | Rep p -> S.repeat (eval p) t) t p

  let parse str =
    let syntax_err fmt = Printf.kprintf failwith fmt in
    let res = ref []
    and stk = ref [] in
    let emit  i  = res := i :: !res
    and enter () = stk := !res :: !stk; res := []
    and leave () = match !stk with
      | r :: s -> stk := s; res := Rep (List.rev !res) :: r
      | []     -> syntax_err "unmatched `]'"
    in String.iter (function
      | '>' -> emit Fwd
      | '<' -> emit Bak
      | '+' -> emit Inc
      | '-' -> emit Dec
      | '.' -> emit Out
      | ',' -> emit Inp
      | '[' -> enter ()
      | ']' -> leave ()
      |  _  -> ()) str;
    if !stk != [] then syntax_err "unmatched `['" else
      List.rev !res

  let format prog =
    let buf = Buffer.create 72
    and len = ref 0 in
    let putc c =
      if !len == 72 || c == '\n' then begin
        Buffer.add_char buf '\n';
        len := 0
      end;
      if c != '\n' then begin
        Buffer.add_char buf c;
        incr len
      end
    in
    let rec go prog = List.iter (function
      | Fwd   -> putc '>'
      | Bak   -> putc '<'
      | Inc   -> putc '+'
      | Dec   -> putc '-'
      | Out   -> putc '.'
      | Inp   -> putc ','
      | Rep p -> putc '['; go p; putc ']') prog
    in go prog; putc '\n'; Buffer.contents buf

end



type tape = Tape of int list * int * int list

module S0 = struct
  type mem = tape

  let mem =
    let rec zeros = 0 :: zeros in
    Tape (zeros, 0, zeros)

  let forward  (Tape (ls, c, r :: rs)) = Tape (c :: ls, r, rs)
  let backward (Tape (l :: ls, c, rs)) = Tape (ls, l, c :: rs)

  let increment (Tape (ls, c, rs)) = Tape (ls, c + 1, rs)
  let decrement (Tape (ls, c, rs)) = Tape (ls, c - 1, rs)


  let output (Tape (_, c, _) as t) =
    output_char stdout (char_of_int (c land 255));
    t

  let input (Tape (ls, c, rs) as t) =
    let c = try int_of_char (input_char stdin) with End_of_file -> c in
    Tape (ls, c, rs)

  let rec repeat p t = match t with
    | Tape (_, 0, _) -> t
    | _             -> repeat p (p t)

end


module S1 (S : SEMANTICS with type mem = tape) = struct
  include S

  let input (Tape (ls, c, rs)) =
    let c = try int_of_char (input_char stdin) with End_of_file -> 0 in
    Tape (ls, c, rs)
end


module S1' (S : SEMANTICS with type mem = tape) = struct
  include S

  let input (Tape (ls, c, rs)) =
    let c = try int_of_char (input_char stdin) with End_of_file -> -1 in
    Tape (ls, c, rs)
end

module S2 (S : SEMANTICS with type mem = tape) = struct
  include S

  let increment (Tape (ls, c, rs)) = Tape (ls, (c + 1) land 255, rs)
  let decrement (Tape (ls, c, rs)) = Tape (ls, (c - 1) land 255, rs)
end


let fill v =
  let rec go l n =
    if n == 0 then l else
    go (v :: l) (pred n)
  in go []

module S3 (N : sig val size : int end)
          (S : SEMANTICS with type mem = tape) = struct
  include S

  let mem = Tape ([], 0, fill 0 N.size)

  let forward  t = match t with
  | Tape (ls, c, r :: rs) -> Tape (c :: ls, r, rs)
  | Tape (_ , _, []     ) -> t

  let backward t = match t with
  | Tape (l :: ls, c, rs) -> Tape (ls, l, c :: rs)
  | Tape ([]     , _, _ ) -> t
end


module S3' (N : sig val size : int end)
           (S : SEMANTICS with type mem = tape) = struct
  include S3 (N) (S)

  let forward  t = match t with
  | Tape (ls, c, r :: rs) -> Tape (c :: ls, r, rs)
  | Tape (ls, c, []     ) ->
    let r :: rs = List.rev (c :: ls) in
    Tape ([], r, rs)

  let backward t = match t with
  | Tape (l :: ls, c, rs) -> Tape (ls, l, c :: rs)
  | Tape ([]     , c, rs) ->
    let l :: ls = List.rev (c :: rs) in
    Tape (ls, l, [])
end


let hello = "
>+++++++++[<++++++++>-]<.>+++++++[<++++>-]<+.+++++++..+++.>>>++++++++[<++++>-]
<.>>>++++++++++[<+++++++++>-]<---.<<<<.+++.------.--------.>>+."

let _ =
  let module S  = S3'(struct let size = 5000 end)(S2(S1(S0))) in
  let module BF = Brainfuck(S) in
  BF.(eval (parse hello)) S.mem

type options =
  | Set_0_on_EOF
  | Set_m1_on_EOF
  | Octet_cells
  | Ignore_out_moves of int
  | Wrap_out_moves of int

module type TapeSem = SEMANTICS with type mem = tape

let semantics options =
  let apply_option opt sem =
    let module S = (val sem : TapeSem) in
    match opt with
      | Set_0_on_EOF -> (module S1(S) : TapeSem)
      | Set_m1_on_EOF -> (module S1'(S) : TapeSem)
      | Octet_cells -> (module S2(S) : TapeSem)
      | Ignore_out_moves n -> (module S3(struct let size = n end)(S) : TapeSem)
      | Wrap_out_moves n -> (module S3'(struct let size = n end)(S) : TapeSem)
  in
  List.fold_right apply_option options (module S0 : TapeSem)

let _ =
  let options = [Wrap_out_moves 20; Octet_cells; Set_0_on_EOF] in
  let module S = (val (semantics options) : TapeSem) in
  let module BF = Brainfuck(S) in
  BF.(eval (parse hello)) S.mem
